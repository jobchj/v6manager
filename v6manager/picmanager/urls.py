# coding:utf8
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^upload/$', 'picmanager.views.upload', name='upload'),
    url(r'^ctrl/$', 'picmanager.views.ctrl', name='ctrl'),
)
