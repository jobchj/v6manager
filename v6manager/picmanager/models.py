# coding: utf8
from django.db import models

# Create your models here.
class Pictures(models.Model):
    p_name = models.CharField(max_length=100)  # 图片名称
    p_position = models.CharField(max_length=10)  # 显示位置
    p_explain = models.CharField(max_length=100)  # 图片描述
    p_href = models.CharField(max_length=200)  # 图片连接
    p_uploaddate = models.DateField(blank=True)  # 上传时间
    p_show = models.BooleanField(default=False)  # 是否显示
    p_img = models.CharField(max_length=200, blank=False)  # 图片地址
    show_id = models.IntegerField()  # 显示顺序

    class Meta:
        db_table = 'pictures'
