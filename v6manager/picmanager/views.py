# coding: utf8
import time

from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from .models import Pictures
from .forms import ImgForm

@login_required
def ctrl(request):
    if request.method == "POST":
        post = request.POST
        set_show(post)
        tp = Pictures.objects.filter(p_position="top").order_by('-id')[:3]
        tfp = Pictures.objects.filter(p_position="top_flash").order_by('-id')[:3]
        # 配置显示的图片
        cp_1 = Pictures.objects.filter(p_position="center", p_show=1).order_by('show_id')
        # 以前的图片
        cp_0 = Pictures.objects.filter(p_position="center", p_show=0).order_by('-id')[:10]
        return render(request, "picmanager/ctrl.html",
                {"tp": tp, "cp_1": cp_1, 'cp_0': cp_0,
                 'tfp': tfp, "msg": "修改成功！"})
    else:
        tp = Pictures.objects.filter(p_position="top").order_by('-id')[:3]
        tfp = Pictures.objects.filter(p_position="top_flash").order_by('-id')[:3]
        # 配置显示的图片
        cp_1 = Pictures.objects.filter(p_position="center", p_show=1).order_by('show_id')
        # 以前的图片
        cp_0 = Pictures.objects.filter(p_position="center", p_show=0).order_by('-id')[:10]
    return render(request, 'picmanager/ctrl.html', {'tp': tp, 'cp_1': cp_1,
                                                    'cp_0': cp_0, 'tfp': tfp})

def set_show(show_list):
    """
    设置图片是否显示, 还有图片显示顺序
    """
    # 设置图片显示顺序
    for k, v in show_list.iteritems():
        img_id = k[8:]
        if img_id:
            flag = img_id.isdigit()
            if flag:
                pic = Pictures.objects.get(pk=img_id)
                pic.show_id = v
                pic.save()
    # 初始化所有图片为不显示
    pics = Pictures.objects.filter(p_show=True)
    for each_pic in pics:
        each_pic.p_show = False
        each_pic.save()
    # 设置需要显示的图片
    show_list = list(show_list)
    for x in show_list:
        if not x.startswith("show_") and not x.startswith("csrf"):
            pic = Pictures.objects.get(pk=x)
            pic.p_show = True
            pic.save()

@login_required
def upload(request):
    if request.method == 'POST':
        form = ImgForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data
            position = data.get('position', '')
            explain = data.get('explain', '')
            href = data.get('href', '#')
            show_id = data.get('show_id', 0)
            img = data.get('image', '')
            name = img.name
            uploaddate = time.strftime("%Y-%m-%d")

            new_pic = Pictures()
            new_pic.p_name = name
            new_pic.p_position = position
            new_pic.p_explain = explain
            new_pic.p_href = href or '#'
            new_pic.p_uploaddate = uploaddate
            new_pic.show_id = show_id
            new_pic.save()

            img_name = '%s_%s' % (new_pic.id, name)
            url = 'http://localhost/v5media/notifyimg/%s' % img_name
            new_pic.p_img = url
            new_pic.save()

            write_pic(img, img_name)
            return render(request, 'picmanager/upload.html',
                          {'form': form, 'msg': '上传成功'})
    else:
        form = ImgForm()
    return render(request, 'picmanager/upload.html', {'form': form})

def write_pic(img, img_name):
    with open('/home/chenhj/test/imgs/%s' % img_name, 'wb+') as destination:
        for chunk in img.chunks():
            destination.write(chunk)
