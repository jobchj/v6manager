# coding=utf-8

import os
from django import forms


class ImgForm(forms.Form):

    '''只负责图片上传以及控制位置，是否显示别处处理。
    '''
    position = forms.ChoiceField(label=u"显示位置:",
                                 choices=[('top', u'顶部通知'),
                                          ('top_flash', u'顶部动画通知'),
                                          ('center', u'中间推广')]
                                 )
    image = forms.ImageField(required=True, label=u"请选择图片:",
                             error_messages={'required': '图片不可为空.'})
    explain = forms.CharField(label=u"图片说明",
                              error_messages={'required': '图片说明不可为空.'})
    href = forms.CharField(label=u"图片连接", required=False)
    show_id = forms.IntegerField(label=u"图片顺序", required=True)

    def clean_image(self):
        data = self.cleaned_data['image']
        file_name_suffix = os.path.splitext(data.name)[1].lower()
        if file_name_suffix not in ['.jpg', '.png', '.bmp', '.gif']:
            raise forms.ValidationError(u"禁止上传此种文件类型。")
        return data
