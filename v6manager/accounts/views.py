# coding: utf8
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login, authenticate, logout as auth_logout
from django.http import HttpResponseRedirect

def login(request):
    err_msg = ''
    if request.user.is_authenticated():
        return HttpResponseRedirect('/picmanager/ctrl/')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user:
            auth_login(request, user)
            return HttpResponseRedirect('/picmanager/ctrl/')
        else:
            err_msg = u"用户名或者密码错误！"
    return render(request, 'accounts/login.html', {'err_msg': err_msg})

@login_required
def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/accounts/login/')
